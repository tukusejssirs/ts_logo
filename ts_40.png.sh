#!/usr/bin/env bash

magick -size 500x500 xc:black -font Ubuntu -fill "#00ff00" -pointsize 400 -gravity Center -annotate 0 "ts" -resize 40x40 -depth 8 ts_40.png