## Logos

|                           Logo                            | Filename     | Used for                               |
|:---------------------------------------------------------:|--------------|----------------------------------------|
|  <img src=./aj_240.png width=40 height=40 alt="aj logo">  | `aj_240.png` | wife’s alter-ego profile picture       |
|    <img src=./cv.png width=40 height=40 alt="cv logo">    | `cv.png`     | `cv` repository picture                |
|   <img src=./git.png width=40 height=40 alt="git logo">   | `git.png`    | `git` repository picture               |
|    <img src=./iB.png width=40 height=40 alt="iB logo">    | `iB.png`     | `bash` repository picture              |
|    <img src=./ob.png width=40 height=40 alt="ob logo">    | `ob.png`     | my official accounts profile picture   |
|   <img src=./psv.png width=40 height=40 alt="psv logo">   | `VN.png`     | `psv` repository picture               |
|    <img src=./rl.png width=40 height=40 alt="rl logo">    | `rl.png`     | `romlit` group picture                 |
|    <img src=./tl.png width=40 height=40 alt="tl logo">    | `tl.png`     | `ts_logo` repository picture           |
|    <img src=./ts.png width=40 height=40 alt="ts logo">    | `ts.png`     | my alter-ego accounts profile picture  |
|    <img src=./VN.png width=40 height=40 alt="VN logo">    | `VN.png`     | `vynatok_velka_noc` repository picture |