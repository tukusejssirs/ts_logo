#!/usr/bin/env bash

magick -size 500x500 xc:black -font Ubuntu -fill "#00ff00" -pointsize 400 -gravity Center -annotate 0 "ts" -resize 32x32 -depth 8 ts_32.png